package model;

import java.util.ArrayList;
import java.util.List;

public final class NGramGenerator {
	
	public static String[] generateNgram( String str ){
		str = str.replace(" ", "");
		List<String> nGrams = new ArrayList<String>();
		for ( int i = 0 ; i < str.length() - 2 ; i++ ) {
			nGrams.add( str.substring(i, i + 3) );
		}
		
		String[] arr = nGrams.toArray(new String[nGrams.size()]);
		return arr;
	}

}
