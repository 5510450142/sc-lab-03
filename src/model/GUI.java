package model;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class GUI {
	
	public GUI() {
		initComponent();
	}
	
	public void initComponent() {
		JFrame frame = new JFrame("Ngram Generator");

	    String word = JOptionPane.showInputDialog(frame, "Input A Sentence");
	    
	    String [] nGrams = NGramGenerator.generateNgram(word);
	    
	    String output = "The Original Sentence : " + word + "\nNgrams : ";
	    
	    for ( String s : nGrams ) {
	    	output += " " + s;
	    }
	    
	    JOptionPane.showMessageDialog(frame, output);
	} 

	 
}
